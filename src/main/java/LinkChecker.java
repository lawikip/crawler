import com.ebay.xcelite.Xcelite;
import com.ebay.xcelite.sheet.XceliteSheet;
import com.ebay.xcelite.writer.SheetWriter;
import com.gargoylesoftware.htmlunit.FailingHttpStatusCodeException;
import com.gargoylesoftware.htmlunit.SgmlPage;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.DomNode;
import com.gargoylesoftware.htmlunit.html.DomNodeList;
import com.gargoylesoftware.htmlunit.html.HtmlAnchor;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import org.apache.commons.logging.LogFactory;

import java.io.File;
import java.util.*;
import java.util.logging.Level;

/**
 * Created by lawi on 7/10/2019.
 */
public class LinkChecker extends Config{

    public static void getElementsUrl(String url) throws Exception {

        LogFactory.getFactory().setAttribute("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.commons.httpclient").setLevel(Level.OFF);

        try (final WebClient webClient = new WebClient()) {
            HtmlPage page = webClient.getPage(url);
            webClient.waitForBackgroundJavaScript(1000);
            webClient.getOptions().setThrowExceptionOnScriptError(false);
            DomNodeList<DomNode> links = page.querySelectorAll("a");
            System.out.println("**************************    Begin  Level one    *************************************");
            Xcelite xcelite = new Xcelite();
            XceliteSheet sheet = xcelite.createSheet("Parent");
            SheetWriter<Parent> writer = sheet.getBeanWriter(Parent.class);
            List<Parent> parents = new ArrayList<Parent>();
            checkStatusCode(links,parents);

            if (Config.property.contains("2")){
                System.out.println("**************************    Begin  Level two    *************************************");
                List<HtmlAnchor> anchors = page.getAnchors();
                for (HtmlAnchor anchor : anchors) {
                   // if(anchor.getHrefAttribute().contains("http")) {
                        HtmlPage page2 = anchor.click();
                        System.out.println("*************links under *********************" +anchor.getHrefAttribute());

                        webClient.waitForBackgroundJavaScript(1000);
                        DomNodeList<DomNode> children = page2.querySelectorAll("a");
                        checkStatusCode(children, parents,anchor.getHrefAttribute());
                    //}
                }
            }
            writer.write(parents);
            xcelite.write(new File("Crawler_doc.xlsx"));
        }
    }

    public static void checkStatusCode(DomNodeList<DomNode> links, List<Parent> parents){
        Set uniqueLinks = new HashSet();

        for (DomNode link : links) {
            if (link.getAttributes().getNamedItem("href")!= null && !uniqueLinks.contains(link.getAttributes().getNamedItem("href"))) {
                SgmlPage newPage = link.getPage();
                try {
                    Parent p = new Parent();
                    p.setParentUrl(String.valueOf(link.getAttributes().getNamedItem("href")));
                    int code = newPage.getWebResponse().getStatusCode();
                    if(code!=200){
                        System.out.println("****Broken Link****"+ link.getAttributes().getNamedItem("href") + " -> Status Code " + code);
                    }else {
                        System.out.println(link.getAttributes().getNamedItem("href") + " -> Status Code " + code);
                    }
                    p.setStatusCode(code);
                    parents.add(p);
                }catch(FailingHttpStatusCodeException e){
                    Parent p = new Parent();
                    p.setParentUrl(String.valueOf(link.getAttributes().getNamedItem("href")));
                    p.setStatusCode(e.getStatusCode());
                    parents.add(p);
                    System.out.println(e.getStatusMessage()+"****Broken Link****"+ link.getAttributes().getNamedItem("href") + " -> Status Code " + e.getStatusCode());
                }
                uniqueLinks.add( link.getAttributes().getNamedItem("href"));

            }
        }
    }
    public static void checkStatusCode(DomNodeList<DomNode> links, List<Parent> parents,String parentUrl){
        Set uniqueLinks = new HashSet();

        for (DomNode link : links) {
            if (link.getAttributes().getNamedItem("href")!= null && !uniqueLinks.contains(link.getAttributes().getNamedItem("href"))) {
                SgmlPage newPage = link.getPage();
                try {
                    Parent p = new Parent();
                    p.setParentUrl(parentUrl);
                    p.setChildUrl(String.valueOf(link.getAttributes().getNamedItem("href")));
                    int code = newPage.getWebResponse().getStatusCode();
                    if(code!=200){
                        System.out.println("****Broken Link****"+ link.getAttributes().getNamedItem("href") + " -> Status Code " + code);
                    }else {
                        System.out.println(link.getAttributes().getNamedItem("href") + " -> Status Code " + code);
                    }
                    p.setStatusCode(code);
                    parents.add(p);
                }catch(FailingHttpStatusCodeException e){
                    Parent p = new Parent();
                    p.setParentUrl(parentUrl);
                    p.setChildUrl(String.valueOf(link.getAttributes().getNamedItem("href")));
                    p.setStatusCode(e.getStatusCode());
                    parents.add(p);
                    System.out.println(e.getStatusMessage()+"****Broken Link****"+ link.getAttributes().getNamedItem("href") + " -> Status Code " + e.getStatusCode());
                }
                uniqueLinks.add( link.getAttributes().getNamedItem("href"));

            }
        }
    }
    public static void main(String args[]) throws Exception {
        if (args.length < 2) {
            System.out.println("Please enter site and crawl lever eg. wwww.test.com 1");
        } else {
            System.out.println("Site -> " + args[0] + " Crawl level -> " + args[1]);
            Config.property =args[1];
            getElementsUrl(args[0]);
        }

    }
}
