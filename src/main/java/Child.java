import com.ebay.xcelite.annotations.Column;

/**
 * Created by layden on 7/11/2019.
 */
public class Child {
    @Column(name="Parent url")
    private String parentUrl;
    @Column (name="Url")
    private String childUrl;

    public String getParentUrl() {
        return parentUrl;
    }

    public void setParentUrl(String parentUrl) {
        this.parentUrl = parentUrl;
    }

    public String getChildUrl() {
        return childUrl;
    }

    public void setChildUrl(String childUrl) {
        this.childUrl = childUrl;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    @Column (name="Status code")

    private int statusCode;
}
