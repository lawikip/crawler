import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * Created by layden on 7/10/2019.
 */
public class Config {
    static String property ="2";
    public void setConfiguration() throws IOException {
        Properties props = new Properties();
        props.load(getClass().getResourceAsStream("/name.properties"));
        property = props.getProperty( "crawl.level" );
    }
}
