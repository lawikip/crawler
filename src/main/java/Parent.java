import com.ebay.xcelite.annotations.Column;

/**
 * Created by layden on 7/11/2019.
 */
public class Parent {

    @Column(name="ParentUrl")
    private String parentUrl;

    @Column (name="ChildUrl")
    private String childUrl;

    @Column (name="Status Code")
    private int statusCode;

    public String getParentUrl() {
        return parentUrl;
    }

    public void setParentUrl(String parentUrl) {
        this.parentUrl = parentUrl;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
    public String getChildUrl() {
        return childUrl;
    }

    public void setChildUrl(String childUrl) {
        this.childUrl = childUrl;
    }


}
