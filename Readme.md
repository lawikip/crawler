# Check broken links

This project checks for broken links using htmlunit to crawl through a page.
An excel sheet is generated for all links checked. It provides two levels one and two. Level one check for links in the initial url provided by user. Level two checks child links by navigating to each link in the initial url.

## Executing

Use crawler.jar generated. Execute command jar

Example 1: java -jar crawl.jar "https://google.com" "1"

Example 2: java -jar crawl.jar "https://google.com" "2"


## Results

An excelsheet will be generated with links checked in the same location as the crawl.jar

## Limitations

Javascript links are not implemented